ics-ans-role-rootfs
===================

Ansible role to install the EEE root file system.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
rootfs_centos7_kernel_version: "3.10.0-693.5.2.el7.x86_64"
rootfs_centos7_kernel_rt_version: "3.10.0-693.5.2.rt56.626.el7.centos.x86_64"
rootfs_centos7_archive: "centos7-ess-rootfs-v0.4.0.tar.bz2"
rootfs_centos7_archive_url: "https://artifactory.esss.lu.se/artifactory/swi-pkg/EEE/centos/{{ rootfs_centos7_archive }}"
rootfs_centos7_nfs_mountpoints:
  - src: "{{ ansible_default_ipv4.address }}:/export/epics"
    mountpoint: /opt/epics
  - src: "{{ ansible_default_ipv4.address }}:/export/startup"
    mountpoint: /opt/startup
  - src: "{{ ansible_default_ipv4.address }}:/export/home"
    mountpoint: /home
  - src: "{{ ansible_default_ipv4.address }}:/export/modules/centos7"
    mountpoint: /lib/modules

rootfs_ifc1210_base_url: https://artifactory.esss.lu.se/artifactory/swi-pkg/EEE/eldk
rootfs_ifc1210_kernel_default: uImage--3.14+git0+09424cee64_3428de7103-r0-ifc1210-20150917083734.bin
rootfs_ifc1210_kernel_images:
  - uImage--3.14+git0+09424cee64_3428de7103-r0-ifc1210-20150917083734.bin
  - uImage--3.14.40_git0_09424cee64_7ce37e045a-r0-ifc1210-20160428191749.bin
  - uImage--3.14.40_git0_09424cee64_7ce37e045a-r0-ifc1210-20170123123858.bin
rootfs_ifc1210_dtb:
  - uImage-p2020ifc1210.dtb
rootfs_ifc1210_bitfiles:
  - "FPGA-bitfiles/ifc1210-default/ifc1210_basic_current_IO.bit"
  - "FPGA-bitfiles/ifc1210-default/ifc1210_iocstandard_current_CENTRAL.bit"
rootfs_ifc1210_archive: ifc1210-ess-rootfs-151203.tar.bz2
rootfs_ifc1210_archive_url: "{{ rootfs_ifc1210_base_url }}/{{ rootfs_ifc1210_archive }}"
rootfs_ifc1210_nfs_mountpoints:
  - src: "{{ ansible_default_ipv4.address }}:/export/epics"
    mountpoint: /opt/epics
  - src: "{{ ansible_default_ipv4.address }}:/export/startup"
    mountpoint: /opt/startup
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-rootfs
```

License
-------

BSD 2-clause
